%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    DATA FILE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('---=== Data save procedure started ===---.');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MATRIX DATA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
i = 0;
i = i+1;
dataset{i}.module = [0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;   % 10
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;   % 20
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;   % 30
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;]; % 34    
       
i = i+1;
dataset{i}.module = [0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1;
                     0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0;   % 10
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;   % 20
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;]; % 25
       
i = i+1;
dataset{i}.module = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0;   % 10
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;   % 20
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;]; % 27
       
i = i+1;
dataset{i}.module = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0; % 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;   % 10
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;   % 20
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0;   % 30
                     0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;
                     0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;   % 40
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;]; %44
       
i = i+1;
dataset{i}.module = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0;   % 10
                     0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;   % 20
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0;
                     0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0;   % 30
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0;   % 40
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0;
                     0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0;   % 50
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;]; % 53
       
i = i+1;
dataset{i}.module = [0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0;   % 10
                     0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                     0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;
                     0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;   % 20
                     0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;]; % 26

i = i+1;
dataset{i}.module =    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0;
                        0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0;
                        0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0;    % 10
                        0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1;
                        0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1;
                        0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1;
                        0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0;    % 20
                        0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0;
                        0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0;
                        0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0;
                        0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;    % 30
                        0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0;
                        0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                        0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                        0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;    % 40
                        0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                        0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;
                        0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0;
                        0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0;
                        0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;    % 50
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;
                        0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0;];  % 53

% packaging data
Problem = cell(1,i);
for j=1:i 
    Problem{j} = struct('Module',{dataset{j}.module});
end
             
save data_matrix;

disp('Saved: MATRIX DATA.');

disp('---=== Data save procedure ended ===---.');
% --- EOF ---
