%% CLUSTERING TOOLBOX - SAMPLE TEST SET
% Clustering Toolbox contains clustering algorithms. Clustering is the task of grouping a set of objects in such a way that objects in the same group (called a cluster) are more similar (in some sense or another) to each other than to those in other groups (clusters).
%
% *Author*: Akos Szoke (aszoke@mit.bme.hu)
%
% *Web site*: <http://www.kese.hu>
%
% *Toolbox Source*: <https://bitbucket.org/aszoke/>
%
%%
dbstop if error;
clear;

load data_matrix;

disp(' ***************************************************** ');
disp(' ******** CLUSTERING ALGORITHMS TESTS: Start ********* ')
disp(' ***************************************************** ');

for i=1:7 
    % -- Reading data one-by-one
    input_matrix = Problem{i}.Module;

    % - plot original matrix
    subplot(1,2,1);
    ii = cat(1,input_matrix,zeros(1,size(input_matrix,2)));
    ii = cat(2,ii,zeros(size(ii,1),1));
    pcolor(ii');
    colormap([1 1 1;.7 .7 .7]);
    shading faceted;
    axis ij

    % -- rank-ordered clustering, where 'rcl' contains the clusters according to the rows
    [romatrix,ro,co,rcl,cl] = rocsym(input_matrix);        
    
    % - plot rank-ordered matrix
    subplot(1,2,2);
    ii = cat(1,romatrix,zeros(1,size(input_matrix,2)));
    ii = cat(2,ii,zeros(size(ii,1),1));
    pcolor(ii');
    colormap([1 1 1;.7 .7 .7]);
    shading faceted;
    axis ij
 
end

disp(' ***************************************************** ');
disp(' ********* CLUSTERING ALGORITHMS TESTS: End ********** ')
disp(' ***************************************************** ');
