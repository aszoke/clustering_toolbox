function [x,fval,exitflag] = distribute_tasks(task_weight, team_cap)
% DISTRIBUTE_TASKS between teams using binary integer linear programming (it uses BINTPROG)
%
% Syntax:
%     [x,v,s] = DISTRIBUTE_TASKS(task_weight, team_cap)
%   Input params:
%      team_cap      - team capacities in column vector
%      task_weight   - weights of teams in row vector
%   Return values:
%      x             - computed optimized values for the unknowns
%      fval          - function value
%      exitflag      - exitflag
%
% -------------------------------------------------------------------------
% Description: 
% We consider the case of R|Pmtn|Cmax.
% To model this problem as a LP, we use nm variables x_ij, 1<=i<=n and 1<=j<=m. Variable x 
% denotes the job j is passed on team i. For example, we would interpret a LP solution with 
% x_1j = 1 and x_2j = 0 as assigning job j to team 1 and not assigning to team 2.
%
% Our objective is to minimize makespan D of the schedule. Recall that the amount of processing
% that job j require -- it runs entirely on machine i -- w_ij. Therefore, we can determine the 
% makespan as: Sum_{j}(w_ij*x_ij).
%
% Now, we have to consider what sort of linear constraints on the x_ij are necessary to ensure 
% that they describe a valid solution on an instance of R|Pmtn|Cmax. 
% 1) All job assignments must be: x_ij \in {0,1}. It results nm constraints.
% 2) Clearly the job j must be assigned to only one team: Sum_{i=1:n}(x_ij)=1. It results n constraints.
%
% As a summary:
% Minimize makespan: min f : f = sum_j(w_ij * x_ij}  for i={1..m}
% S.t.
%  x_ij >=0 �s integer  for i=1..m,j=1..n  -- 1) fraction of jobs must be nonnegative (n*m constraints)
%  sum_i(x_ij) = 1      for j=1..n         -- 2) we must fully process each job on a machine (n constraints)
%  sum_j(w_ij * x_ij) <= D  for i=1..m     -- 3) sum job on every machine
%  must be smaller than the D
% -------------------------------------------------------------------------
%
% Complexity: 
% Space     : 
%
% Reference:???
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: BINTPROG

% Copyright 2006-2009

% -- input checking --

% -- function body --

m = length(team_cap);                       % number of teams
n = length(task_weight);                    % number of tasks

% -- matrix of constraints (A) and objective function (f)
vndx = 0;
A1 = [];
A2 = [];
f = [];                                     % parameters of the objective function
for j=1:n
    for k=1:m
        vndx = vndx + 1;
        f(vndx) = task_weight(j);
        A1(j,(j-1)*m+k) = 1;                % constraint 2)
        A2 = [A2,zeros(m,1)];
        A2(k,(j-1)*m+k) = 1;                % constraint 3)
    end
end

for k=1:m
    A2(k,:) = A2(k,:) .* f;
end
A   = [A1 ; A2];

% -- development effort constraints (b)
b1  = ones(n,1);
b   = [b1;...                               % one task must be impemented by one team only
       team_cap];                           % available resource capacities in teams

% -- calculation with 'bintprog' (Matlab) solver
disp(' ... TEST ''bintprog'' solver: Start ... ')
tic;
[x,fval,exitflag] = bintprog(f*(-1),A,b);       % negative for minimization!
toc;
if exitflag == 1
    disp(['Allocation of tasks  : ' num2str(x')]);
    disp(['Delivered weights    : ' num2str(fval)]);
    for ii=1:size(A2,1)
        [r,c,val] = find(A2(ii,:).*(x'));       % assigned tasks to the team ii
        disp(['Distribution on team (' num2str(ii) ') : ' num2str(val) ' => Sum:' num2str(sum(val))]);
    end;
else
    error('Some problems are occured.')
end

% --- EOF ---
