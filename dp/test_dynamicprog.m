%% DYNAMIC PROGRAMMING - SAMPLE TEST SET
%
% *Author*: Akos Szoke (aszoke@mit.bme.hu)
%
% *Web site*: <http://www.kese.hu>
%
% *Toolbox Source*: <https://bitbucket.org/aszoke/>
%
%%
dbstop if error;
clear;

%% MATRIXMUL
% set multipliable matrices
A1 =rand(30,35);
A2 =rand(35,15);
A3 =rand(15,5);
A4 =rand(5,10);
A5 =rand(10,20);
A6 =rand(20,25);

% construct a structure for the algorithm
A = {A1, A2, A3, A4, A5, A6};

% simple (not optimized) case
[m1,c1] = matrixmul0(A)

% dynamic programming case
[m2,c2] = matrixmul(A)

% comparing the cost of multiplication: the greater value the better the
% dynamic programming solution
c1/c2

%% LCS

X = ['A' 'B' 'C' 'B' 'D' 'A' 'B'];
Y = ['B' 'D' 'C' 'A' 'B' 'A'];
%Y = ['B' 'C' 'D' 'B'];

[Z, s] = lcs(X,Y)



