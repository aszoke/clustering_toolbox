# Dynamic Programming Algorithms within Clustering Toolbox

### Features

* **lcs** - dynamic programming solution for the optimal (minimal cost of) longest common subsequence (LCS) problem
* **matrixmul** - dynamic programming solution for the optimal (minimal cost of) matrix multiplication problem
* **matrixmul0** - the simplest solution for the matrix multiplication problem
