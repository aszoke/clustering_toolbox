function [Z,seq] = lcs(X,Y)
%LCS dynamic programming solution for the optimal (minimal cost of) longest common subsequence (LCS) problem
% E.g. if X = "ABCBDAB" and Y = "BCDB" then Z = 2,3,5,7
%
% Syntax:
%     seq = lcs(X,Y) 
%   Input params:
%     X - string 1
%     Y - string 2
%   Return values:
%     seq - sequence of indeces of the longest common subsequence
%
% Complexity: Ordo(mn)
% Space     : Ordo(mn)
%
% 1) optimal substructure: LCS of two sequences contains within it an LCS
% of prefixes of the two sequences (recursive solution)
%
% 2) overlapping subproblems: (bottom up solution)
%
% 3) memoization: 
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 16, Dynamic programming
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: p.314

% See also: -

% Copyright 2006-2008

% -- input checking -- 

% -- function body --

m = length(X);
n = length(Y);
%   c[i,j] contains the length of the X_i and Y_j 
c = zeros(m+1,n+1);
%   b[i,j] contains remarks for memoisation: how the solution was bulit
b = zeros(m+1,n+1);

% computing the length of the common subsequence
for i=2:m+1
    for j=2:n+1
        if isequal(X(i-1),Y(j-1))       % if the two characters are equal then increase the length
            c(i,j) = c(i-1,j-1) + 1;
            b(i,j) = 3;     % "upper-left"             
        else if c(i-1,j) >= c(i,j-1)
                c(i,j) = c(i-1,j);
                b(i,j) = 1; % "upper"
            else
                c(i,j) = c(i,j-1);
                b(i,j) = 2; % "left"
            end;
        end;
    end;
end;

global Z;
global seq;
Z = [];
seq = [];
i=m+1;
j=n+1;

constructLCS(X,b,i,j);
return;

% constructing the subsequence II. -- with iterative method 
while (i~=1)&&(j~=1)
    switch b(i,j)
        case 1
            i = i-1;
        case 2
            j = j-1;
        case 3
            seq = vertcat(i-1,seq)    % concatenating the indeces
            Z = strvcat(X(i-1),Z)     % concatenating the string
            i = i-1;
            j = j-1;
    end;
end;

% --------------------- SUBFUNCTION ----------------------------
% constructing the subsequence I. -- with recursive method 
%
function constructLCS(X,b,i,j)
global Z;
global seq;
    if (i~=1) && (j~=1)
        switch b(i,j)
            case 1
                constructLCS(X,b,i-1,j);
            case 2
                constructLCS(X,b,i,j-1);
            case 3
                seq = vertcat(i-1,seq)    % concatenating the indeces
                Z = strvcat(X(i-1),Z)     % concatenating the string            
                constructLCS(X,b,i-1,j-1);
        end;
    end;


