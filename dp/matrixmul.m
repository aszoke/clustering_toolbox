function [res,cost] = matrixmul(A)
%MATRIXMUL dynamic programming solution for the optimal (minimal cost of) matrix multiplication problem
%
% Syntax:
%     res = matrixmul(A) 
%   Input params:
%     A - matrix chain
%   Return values:
%     res - result of the multiplication
%     cost - cost of multiplication
%
% ========================================================================
%                       Comment on the operation
% ========================================================================
% The key characteristics are realized by: 
% 1) optimal substructure: -> optimal parenthesitation of A_1...A_n problem contains within it optimal solutions to subproblem instances
%
% 2) overlapping subproblems: -> item level multiplication leads to the matrix chain multiplication
%
% 3) memoization: -> variable 's' to construct the multiplication (top-down)
% ========================================================================
%
% Complexity: O(n^3)
% Space     : O(n^2)
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 16, Dynamic programming
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: p.307

% See also: -

% Copyright 2006-2008

% -- input checking -- 

for i=1:length(A)-1
    if size(A{i},2)~=size(A{i+1},1)
        error('Size mismatch in the matrix chain!');
    end;
end;

% -- function body --

p(1)=size(A{1},1);
for i=1:length(A)
    p(i+1) = size(A{i},2);
end;

n = length(p)-1;    % number of matrices

% minimal cost VALUE matrix. It contains the minimal cost of computation between
% Ai and Aj matrices (with different i-j distances). But since i<j therefore 
% it is an upper triangular matrix.
% m(i,j) contains the minimal cost of multiplication of A[i..j]. 
% E.g. m(2,5)= min( 
%                   m(2,2)+m(3,5)+p(1)*p(2)*p(5),
%                   m(2,3)+m(4,5)+p(1)*p(3)*p(5),
%                   m(2,4)+m(5,5)+p(1)*p(4)*p(5)
%                  )
% m(1,n) contains the minimal cost of multiplication of all matrices
m = zeros(n,n);    

% minimal cost PLACE matrix. Between i and j which is the minimum cost k:
% it should be multipied to serve minimal multiplication cost.
% (s[i,j] equals to k where we can split m[i,j] to obtain optimal paranthesization)
s = zeros(n,n);     

% during the cource of the operation (bottom-up approach):
% - first we select l=2 distance between i and j to compute the cost of computations
% - then select the minimal one (m[i,j]=q) and its position (s[i,j]=k)
% - finally increase the distance to 3,4,...
% Notes:
% -> memoization: is realized by s[], and m[]  

global cost;
% cost of computation is made up two things: cost of ddetermining the
% optimal sequence plus the cost of computation
cost = 0;           

% determining the optimal matrix chain order:
for l=2:n           % distance between i and j
    for i=1:n-l+1
        j = i+l-1;
        m(i,j)=inf; % initialization for the minimum selection
        for k=i:j-1 % enumerating chains between i..j with distance l
            % cost of computation: calculates the distance 2, 3, ... costs
            % A[i..k]*A[k+1...j] can be calculated by: 
            q = m(i,k) ...      % cost of previous multiplication of A[i..k] (1)
                + m(k+1,j) ...  % cost of previous multiplication of A[k+1..j] (2)
                + p(i)*p(k+1)*p(j+1);   % cost of multiplication of (1) and (2)
            if q < m(i,j)   % select the minimum cost
                m(i,j) = q;
                s(i,j)=k;   % records the value of k of A[i..j]
            end;
            cost = cost + 1;    % cost of computing the optimal sequence
        end;
    end;
end;

res = matrix_chain_multiply(A,s,1,n);

% --------------------- SUBFUNCTION ----------------------------
% constructiong the optimal solution from computed information
% it follows top-to-bottom trajectory. E.g.:
% [1,6] -> 3 ()
% [1,3] -> 1
% [1,1] -> A[1]
% [2,3] -> 2 
% [2,2] -> A[2] 
% [3,3] -> A[3]
%  => A[1]*(A[2]*A[3])
% ...
%
function m = matrix_chain_multiply(A,s,i,j)
global cost;

    if j>i
        x = matrix_chain_multiply(A,s,i,s(i,j));
        y = matrix_chain_multiply(A,s,s(i,j)+1,j);
        m = x * y;
        % multipication sequence:
        disp([num2str(i) '*' num2str(j)])
        cost = cost + size(x,1)* size(y,2) * size(x,2); % cost of multipication
    else
        m = A{i};
    end;
