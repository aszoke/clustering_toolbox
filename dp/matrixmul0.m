function [res, cost] = matrixmul0(A)
%MATRIXMUL0 the simplest solution for the matrix multiplication problem
%
% Syntax:
%     res = matrixmul(A) 
%   Input params:
%     A - matrix chain
%   Return values:
%     res - result of the multiplication
%     cost - cost of multiplication
%
% Complexity: O(n^2)
% Space     : O(n^2)
%
% Reference:
% (Cormen:alg)
%   Cormen et. al. Introduction to Algorithms, MIT Press, 2000,
%   Chapter 16, Dynamic programming
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: p.303

% See also: MATRIXMUL

% Copyright 2006-2008

% -- input checking -- 

for i=1:length(A)-1
    if size(A{i},2)~=size(A{i+1},1)
        error('Size mismatch in the matrix chain!');
    end;
end;

% -- function body --

res = A{1};
cost = 0;       % cost of computation (number of multiplication)
for i=2:length(A)
    [res,c] = matrix_multiply(res,A{i});
    cost = cost + c;
end;

% --------------------- SUBFUNCTION ----------------------------
% matrix multiplication
%
function [m,cost] = matrix_multiply(A1,A2)

for i=1:size(A1,1)        % rows of A1
    for j=1:size(A2,2)    % columns of A2
        m(i,j) = 0;
        for k=1:size(A1,2)    % columns of A1 (=rows of A2)
            m(i,j) = m(i,j) + A1(i,k)*A2(k,j);
        end;
    end;
end;
 cost = size(A1,1)* size(A2,2) * size(A1,2);
