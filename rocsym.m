function [romat,roworder,colorder,rowcluster,clusterpos] = rocsym(inp_mat)
%ROCSYM computes rank-ordered clustering for large matrices
%
% Description: It orders the elements (phase 1) to the left-up corner and clustering
% them (phase 2). In the return value, roworder/colorder points out that
% which item serial number goes into the romat serial number. E.g. if 
% romat = [3,21,34...] means that the 1st, 2nd,... row/column is the 3rd, 21st,... row/column
% in the original matrix.
% 
% Syntax:
%     [romat,roworder,colorder,rowcluster,clusterpos] = ROCSYM(inp_mat)
%   Input params:
%      inp_mat      - input matrix
%   Return values:
%      romat         - rank-ordered matrix
%      roworder      - row order according to the original matrix
%      colorder      - column order according to the original matrix
%      rowcluster    - clusters of rows (clusters are given back as row-sets!)
%      clusterpos    - positions of clusters
%
% Complexity: 
% Space     : 
%
% Reference:???
%
% Author: Akos Szoke (aszoke@mit.bme.hu)
% Example: -
% See also: -
% Copyright 2006-2009

tStart=tic;
% -- input checking --

% -- function body --
global g_roworder;     % row order according to the original matrix
global g_colorder;     % column order according to the original matrix

% --- Phase 1: rank-ordering the matrix. Rank ordering is accomplished by
% ordering the matrix along the dimension in turn.
imat = inp_mat;
g_roworder = [1:size(imat,1)];
g_colorder = [1:size(imat,2)];

isSorted = false;
d = 1;      % using for alter the dimension of sorting
while (~isSorted)
    d = ~d;
    romat = sortmatrix(imat,d + 1);
    if (romat==imat)
        isSorted = true;
    else
        imat = romat;
    end
end

% --- Phase 2: clustering the rank-ordered matrix
omat = romat;
clrowstart = 0;             % start row (!) coordinate position of the cluster 
clcolstart = 0;             % start column coordinate (!) position of the cluster 
clusterpos = [];               % contains the clustering positions
rowcluster = [];            % clustered rows
i = 0;                      % index for rowcluster vector
while(~isempty(omat))
    isCluster = false;
    % if there are empty rows than it is trimed
    while(isempty(find(omat(1,:))))
        omat(1,:) = [];         % deleting the empty row
        if isempty(omat)
            roworder = g_roworder;
            colorder = g_colorder;
           return; 
        end
        clrowstart = clrowstart + 1;
    end
    rs = 1;
    while(~isCluster)
        [~,cols]  = find(omat(rs,:));        % finding column items according to the rows
        cols = unique(cols);
        maxcol = max(cols);
        [rows,~]  = find(omat(:,cols));      % finding row items according to the columns
        rows = unique(rows);
        maxrow = max(rows);
        if (isempty(setdiff(rows,rs)))              % id rows aren't extended by the columns
            isCluster = true;
        else
            rs = rows;
        end
    end
    rowlen = size(omat,1);
    collen = size(omat,2);
    omat = omat(maxrow+1:rowlen, maxcol+1:collen);  % trimming the cluster out from the input matrix
    
    % calculating the start and end position of the clusters
    clstart = [clrowstart, clcolstart];
    clrowend = clrowstart+maxrow; 
    clcolend = clcolstart+maxcol;    
    clend   = [clrowend, clcolend];
    clusterpos = [clusterpos; clstart, clend];
    
    % storing row clusters
    i = i+1;    
    rowcluster{i} = [clrowstart+1:clrowend];
    
    % displacing the start of the cluster for next cluster computation
    clrowstart = clrowend; 
    clcolstart = clcolend;    
end

roworder = g_roworder;
clear global g_roworder;
colorder = g_colorder;
clear global g_colorder;

disp(['ROCSYM is taken ' num2str(toc(tStart)) ' sec.']);
end

% --------------------- SUBFUNCTION ----------------------------
function smat = sortmatrix(mat,dim)
%SORTMATRIX sorts matrix along its dimension according to their values which is interpreted as binary values 
%
% Syntax:
%     smat = SORTMATRIX(mat,dim)
%   Input params:
%      mat          - items in the matrix
%      dim          - sorting dimension
%   Return values:

% -- input checking --

% -- function body --

global g_roworder;
global g_colorder;

len = size(mat,dim);           % determine the size of the dimension
value = cell(len,1);           % initialization of rows/column values

% calculating row value of each row by interpreting rows as binary numbers
for i = 1:1:len
    % deciding whether rows or colums are calculated
    switch dim
        case 1
            val = mat(i,:);
        case 2
            val = mat(:,i)';
        otherwise
            error('Matrix dimension value must be 1 or 2!');
    end
    value{i} = val;
end

[~,ndx] = sortsym(value); 

switch dim
    case 1
        smat = mat(ndx,:);
        g_roworder = g_roworder(:,ndx);
    case 2
        smat = mat(:,ndx);
        g_colorder = g_colorder(:,ndx);
    otherwise
        error('Matrix dimension value must be 1 or 2!');
end
end

% --------------------- SUBFUNCTION ----------------------------
function [b,ix] = sortsym(a)
%SORTSZM sort based on strings as symbolic values. Mode of soring is descending.
%
% Description: the sym.sort function provides very basic functionality, therefore this procedure is implemented.
% 
% Syntax:
%     [B,IX] = sortsym(a,mode)
%   Input params:
%      a             - input vector with string elements
%   Return values:
%      b             - sorted vector
%      ix            - indeces of the input vector

% -- input checking -- 
    
% -- function body --

b = a;
len = length(b);
ix = [1:len];

% using bubble sort
isSwapped = true;
while(isSwapped)
    isSwapped = false;
    for j=1:len-1
        if isGreater(b{j+1},b{j})
            swap = b{j};
            swapix = ix(j);
            b{j} = b{j+1};
            ix(j) = ix(j+1);
            b{j+1} = swap;
            ix(j+1) = swapix;
            isSwapped = true;
        end
    end
end
end

% --------------------- SUBFUNCTION ----------------------------
function bool = isGreater(sym1,sym2)
%ISGREATER determines whether a symbolic variable is greater than an other symbolic variable - they must be integer numbers
%
% Syntax:
%     bool = isGreater(sym1,sym2)
%   Input params:
%      sym1          - symbolic variable 1
%      sym2          - symbolic variable 2
%   Return values:
%      bool         - its value is True if sym1 is greater than sym2

% -- input checking -- 

if ~isequal(length(sym1),length(sym2))
    error('Length of symbolic variables must be equal!')
end

% -- function body --

for i=1:length(sym1)
    if sym1(i) > sym2(i)
        bool = true;
        return;
    elseif sym1(i) < sym2(i)
        bool = false;
        return;
    end
end
bool = false;   % the two variables are equal   
end

% --- EOF ---
