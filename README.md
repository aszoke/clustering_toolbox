# Clustering Toolbox

## About

Clustering Toolbox contains clustering algorithms. Clustering is the task of grouping a set of objects in such a way that objects in the same group (called a cluster) are more similar (in some sense or another) to each other than to those in other groups (clusters).

**Author**: Akos Szoke <aszoke@mit.bme.hu>

**Web site**: [http://www.kese.hu](http://www.kese.hu)

**Toolbox Source**: [https://bitbucket.org/aszoke/](https://bitbucket.org/aszoke/)

### Features

* **distribute_tasks** - distribute tasks between teams using binary integer linear programming
* **roc** - computes the rank-ordered clustering (Vector dimensions must be 52 bits or less!)
* **rocsym** - ROCSYM computes rank-ordered clustering for large matrices

### Dependencies

* In  a sense that the toolbox can generate _dot_ file that can be visualized e.g. with [Graphviz](http://www.graphviz.org/).
* [bintbrog](http://www.mathworks.com/help/optim/ug/bintprog.html) - only for [distribute_tasks.m](./clustering_toolbox/src/master/distribute_tasks.m)
* [symbolic](http://www.mathworks.com/products/symbolic/) - only for [rocsym.m](./clustering_toolbox/src/master/rocsym.m)

## Usage

using the functions in the feature list

### Example

 * [test_clustering_toolbox.m](./clustering_toolbox/src/master/test_clustering_toolbox.m)

The generated log of the example can be found in
[test_clustering_toolbox.html](https://bitbucket.org/aszoke/clustering_toolbox/raw/master/html/test_clustering_toolbox.html).

**Note**: *Due to the Bitbucket HTML preview restriction (see [HTML rendering for generated doc](https://bitbucket.org/site/master/issue/6353/html-rendering-for-generated-doc)), in order to view the Test run samples in HTML, use should use the [GitHub & BitBucket HTML Preview](http://htmlpreview.github.io/) service. (Copy the link below and paste into the text box that can be found in the service.)*

### Test data

The test data can be generated with [generate_data.m](./clustering_toolbox/src/master/generate_data.m)

* [data_matrix.mat](./clustering_toolbox/src/master/data_matrix.mat)

## License

(The MIT License)

Copyright (c) 2011 Akos Szoke

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.