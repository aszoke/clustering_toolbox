function [romat,roworder,colorder,rowcluster,clusterpos] = roc(inp_mat)
%ROC computes the rank-ordered clustering (Vector dimensions must be 52 bits or less!)
%
% Description: It orders the elements (phase 1) to the left-up corner and clustering
% them (phase 2). In the return value, roworder/colorder points out that
% which item serial number goes into the romat serial number. E.g. if 
% romat = [3,21,34...] means that the 1st, 2nd,... row/column is the 3rd, 21st,... row/column
% in the original matrix.
% 
% Syntax:
%     [romat,roworder,colorder,rowcluster,clusterpos] = ROC(inp_mat)
%   Input params:
%      inp_mat      - input matrix
%   Return values:
%      romat         - rank-ordered matrix
%      roworder      - row order according to the original matrix
%      colorder      - column order according to the original matrix
%      rowcluster    - clusters of rows (clusters are given back as row-sets!)
%      clusterpos    - positions of clusters
%
% Complexity: 
% Space     : 
%
% Reference:???
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% See also: -

% Copyright 2006-2009

% -- input checking --

if (size(inp_mat,2)>52) || (size(inp_mat,1)>52)
    error('Vector dimensions must be 52 bits or less!')
end   
    
% -- function body --
global g_roworder;     % row order according to the original matrix
global g_colorder;     % column order according to the original matrix

% --- Phase 1: rank-ordering the matrix. Rank ordering is accomplished by
% ordering the matrix along the dimension in turn.
imat = inp_mat;
g_roworder = [1:size(imat,1)];
g_colorder = [1:size(imat,2)];

isSorted = false;
d = 1;      % using for alter the dimension of sorting
while (~isSorted)
    d = ~d;
    romat = sortmatrix(imat,d + 1);
    if (romat==imat)
        isSorted = true;
    else
        imat = romat;
    end
end

% --- Phase 2: clustering the rank-ordered matrix
omat = romat;
clrowstart = 0;             % start row (!) coordinate position of the cluster 
clcolstart = 0;             % start column coordinate (!) position of the cluster 
clusterpos = [];               % contains the clustering positions
rowcluster = [];            % clustered rows
i = 0;                      % index for rowcluster vector
while(~isempty(omat))
    isCluster = false;
    % if there are empty rows than it is trimed
    while(isempty(find(omat(1,:))))
        omat(1,:) = [];         % deleting the empty row
        if isempty(omat)
            roworder = g_roworder;
            colorder = g_colorder;
           return; 
        end
        clrowstart = clrowstart + 1;
    end
    rs = 1;
    while(~isCluster)
        [~,cols]  = find(omat(rs,:));        % finding column items according to the rows
        cols = unique(cols);
        maxcol = max(cols);
        [rows,c]  = find(omat(:,cols));      % finding row items according to the columns
        rows = unique(rows);
        maxrow = max(rows);
        if (isempty(setdiff(rows,rs)))              % id rows aren't extended by the columns
            isCluster = true;
        else
            rs = rows;
        end
    end
    rowlen = size(omat,1);
    collen = size(omat,2);
    omat = omat(maxrow+1:rowlen, maxcol+1:collen);  % trimming the cluster out from the input matrix
    
    % calculating the start and end position of the clusters
    clstart = [clrowstart, clcolstart];
    clrowend = clrowstart+maxrow; 
    clcolend = clcolstart+maxcol;    
    clend   = [clrowend, clcolend];
    clusterpos = [clusterpos; clstart, clend];
    
    % storing row clusters
    i = i+1;    
    rowcluster{i} = [clrowstart+1:clrowend];
    
    % displacing the start of the cluster for next cluster computation
    clrowstart = clrowend; 
    clcolstart = clcolend;    
end

roworder = g_roworder;
clear global g_roworder;
colorder = g_colorder;
clear global g_colorder;
end

% --------------------- SUBFUNCTION ----------------------------
function smat = sortmatrix(mat,dim)
%SORTMATRIX sorts matrix along its dimension according to their values which is interpreted
%as binary values 
%
% Syntax:
%     smat = SORTMATRIX(mat,dim)
%   Input params:
%      mat          - items in the matrix
%      dim          - sorting dimension
%   Return values:
%      smat         - sorted matrix
%
% Complexity: 
% Space     : 
%

global g_roworder;
global g_colorder;

len = size(mat,dim);           % determine the size of the dimension
value = zeros(len,1);          % initialization of rows/column values
strvalue = cell(len,1);
% calculating row value of each row by interpreting rows as binary numbers
for i = 1:1:len
    % deciding whether rows or colums are calculated
    switch dim
        case 1
            strval = num2str(mat(i,:));
        case 2
            strval = num2str(mat(:,i)');
        otherwise
            error('Matrix dimension value must be 1 or 2!');
    end
            strval = strrep(strval,' ',''); % deleting spaces from the string
            val = bin2dec(strval);
            value(i) = val;              % values vector
            strvalue{i} = strval;
end

[~,ndx] = sort(value,'descend');

switch dim
    case 1
        smat = mat(ndx,:);
        g_roworder = g_roworder(:,ndx);
    case 2
        smat = mat(:,ndx);
        g_colorder = g_colorder(:,ndx);
    otherwise
        error('Matrix dimension value must be 1 or 2!');
end

end

% --- EOF ---
