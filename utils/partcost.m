function cost = partcost(M,ndx)
% PARTCOST calculates the cost of partitioning i.e. sum of cutted edges
%
% Syntax:
%     partcost(M,ndx)
%   Input params:
%      M              - edge weighted matrix
%      ndx            - indices of partitions
%   Return values:
%      cost           - cost of partitioning 
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% Copyright 2006-2010

% -- input checking -- 

% -- function body --

we = 0; % weight of edges of the partitioned graphs
for i=1:max(ndx)
    jndx = find(ndx == i);
    we = we + sum(sum(M(jndx,jndx))); 
end
cost = (sum(sum(M)) - we) / 2; % halved because it is nondirected

end

% --- EOF ---