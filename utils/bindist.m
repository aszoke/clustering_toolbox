function [S,D] = bindist(a,type)
%BINDIST computes different similarity coefficients and distances of binary vectors. Note: 'Hamming distance' = 1 - 'Simple similarity'
%

% Syntax:
%     [S,D] = bindist(a,type)
%   Input params:
%      a             - input vector (same width, minimum 2 elements)
%      type          - type = {'Jaccard', 'Simple'}
%               
%   Return values:
%      S             - similarity coefficient matrix between all the elements
%      D             - distance coefficient matrix between all the elements
%
% Complexity: 
% Space     : 
%
% Reference: 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% Copyright 2006-2010

tStart=tic;
% -- input checking -- 

% number of columns -> number of comparable vectors
numvec = size(a,2);
len = size(a,1);

if numvec < 2
    error('Number of vectors must be greater than 2!');
end;

% -- function body --

S = zeros(numvec);
D = zeros(numvec);

for i=1:numvec
    for j=1:numvec
        p = 0;  % number of variables that positive for both objects
        q = 0;  % number of variables that positive for the i th objects and negative for the j th object
        r = 0;  %  number of variables that negative for the i th objects and positive for the j th object
        s = 0;  % number of variables that negative for both objects
        ci = a(:,i);
        cj = a(:,j);
        for t=1:len
            if (ci(t)==1) && (cj(t)==1)
                p = p+1;
            elseif (ci(t)==1) && (cj(t)==0)
                q = q+1;
            elseif (ci(t)==0) && (cj(t)==1)    
                r = r+1;
            elseif (ci(t)==0) && (cj(t)==0) 
                s = s+1;
            else
                error('The vector is not in binary format!');
            end
        end
        
        % Comment on Jaccard's distance:
        % The existence of in "simple matching" of zeros makes no sense because it
        % represents double absence. For example, if the negative value is not 
        % important, counting the non-existence in both objects may have no 
        % meaningful contribution to the similarity or dissimilarity.
        
        
        switch upper(type)
            case 'SIMPLE' % HAMMING DISTANCE = q + r can be derived from it
                S(i,j)=(p+s)/(p+q+r+s);
            case 'JACCARD'
                if p == 0       % if there is no similar property there similarity is 0!
                    if (q+r) == 0
                        S(i,j) = 1;
                    else
                        S(i,j) = 0;
                    end
                else
                    S(i,j)=p/(p+q+r);
                end
            otherwise
                error('Unknown distance type!');
        end;
        D(i,j)=1-S(i,j);
    end
end

disp(['BINDIST is taken ' num2str(toc(tStart)) ' sec.']);

end

% --- EOF ---
