# Utils of the Clustering Toolbox

### Features

* **bindist** - computes different similarity coefficients and distances of binary vectors. Note: 'Hamming distance' = 1 - 'Simple similarity'
* **partcost** - calculates the cost of partitioning i.e. sum of cutted edges
* **spymat** - plots sparcity pattern of the input quadratic matrix
* **wgraph2dot** - writes distance of elements to dot file
