function wgraph2dot(D,clndx,DatasetName,algorithm,isConvert,isDrawClusters,isDrawEdges,isDrawWeight)
% WGRAPH2DOT writes distance of elements to dot file
%
% Syntax:
%     wgraph2dot(D,clndx,DatasetName,algorithm,isConvert,isDrawClusters,isDrawEdges,isDrawWeight)
%   Input params:
%      D              - distance between elements 
%      clndx          - cluster indeces of each element
%      DatasetName    - data set name
%      algorithm      - used algorithm for drawing
%      isConvert      - whether external 'dot' converter is launched
%      isDrawClusters - whether clusters are drawn
%      isDrawEdges    - whether edges are drawn
%      isDrawWeight   - whether edge weights are drawn
%   Return values:
%      -            - 
%
% Complexity: -
% Space     : -
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% Copyright 2006-2009

tStart=tic;
% -- input checking -- 
if ~isempty(clndx)
    if (length(clndx) ~= size(D,1)) || ...
       (length(clndx) ~= size(D,2))
        error('Size mismatch!');
    end
end

% -- function body --

% find vertices and their distances
[DFrom,DTo,DVal] = find(D);
if isempty(clndx)
    clndx = ones(size(D,1),1);
end
[~,~,cl] = find(clndx);
clind = unique(cl);

NODEPROP = 'style=filled,shape=record,fontsize=20,height=0.3,width=0.5';

fid = fopen([DatasetName '.dot'],'w');
fprintf(fid,'graph G { \n');
fprintf(fid,'overlap = scale; \n');
fprintf(fid,'splines = true; \n');
fprintf(fid,'orientation = portrait; \n'); % tries to use this value
fprintf(fid,'center = 1; \n');
fprintf(fid,'size="10,40"; \n');            % tries to use this value
fprintf(fid,['node [' NODEPROP ']; \n']);

% writing distance of nodes to the output file
if ~isempty(DFrom)
    if isDrawClusters
        for cli = 1:length(clind)
            ind = find(clndx == clind(cli));
            fprintf(fid,['subgraph cluster_' num2str(clind(cli)) ' { \n']);
            fprintf(fid,'color=black; \n');
            for i=1:length(ind)
                fprintf(fid,[num2str(ind(i)) '; \n']);    
            end
            fprintf(fid,['label = "group #' num2str(clind(cli)) '"; \n']);
            fprintf(fid,'} \n');
        end
    end
    if isDrawEdges
        for i=1:length(DFrom)
            if isDrawWeight
                lbl = num2str(DVal(i));
            else
                lbl = '';
            end
            fprintf(fid,[num2str(DFrom(i)) ' -- ' num2str(DTo(i)) ' [len=' num2str(DVal(i)) ', label="' lbl '"]; \n']);    
        end
    end
end

fprintf(fid,'}');

if isConvert
    switch algorithm
        case 'dot'
            system(['dot -Tpng ' DatasetName '.dot -o ' DatasetName '.png']);
        case 'neato'
            system(['neato -Tpng ' DatasetName '.dot -o ' DatasetName '.png']);
        otherwise
            error('Unknown algorithm!');
    end
end

fclose(fid);

disp(['WGRAPH2DOT is taken ' num2str(toc(tStart)) ' sec.']);

end
% --- EOF ---