function spymat(input_matrix)
%SPYMAT plots sparcity pattern of the input quadratic matrix
%
% Syntax:
%     spymat(input_matrix)
%   Input params:
%      input_matrix - input matrix
%   Return values:
%      - 
%
% Complexity: 
% Space     : 
%
% Reference: 
%
% Author: Akos Szoke (aszoke@mit.bme.hu)

% Example: -

% Copyright 2006-2010

% -- input checking -- 

% calculating colormap
[~,~,v] = find(input_matrix);
numcolor = length(unique(v));
m = [];
for ci=1:numcolor
    m = [ci * [1/numcolor 1/numcolor 1/numcolor];m];
end

ii = cat(1,input_matrix,zeros(1,size(input_matrix,2)));
ii = cat(2,ii,zeros(size(ii,1),1));
pcolor(ii');
colormap(m);
shading faceted; 
axis ij

end

% --- EOF ---

